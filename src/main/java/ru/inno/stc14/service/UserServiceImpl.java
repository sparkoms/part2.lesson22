package ru.inno.stc14.service;

import ru.inno.stc14.dao.UserDAO;
import ru.inno.stc14.dao.jdbc.UserDAOImpl;
import ru.inno.stc14.entity.User;

import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;

public class UserServiceImpl implements UserService {
    private Logger logger = Logger.getLogger(UserService.class.getName());
    private final UserDAO userDAO;

    public UserServiceImpl(Connection con) {
        userDAO = new UserDAOImpl(con);
    }

    @Override
    public List<User> getList() {
        return userDAO.getList();
    }

    @Override
    public boolean addUser(String name, String password) {
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        return userDAO.addUser(user);
    }

    @Override
    public boolean isExistUser(String name, String password) {
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        return userDAO.isExistUser(user);
    }
}