package ru.inno.stc14.servlet;

import ru.inno.stc14.service.UserService;
import ru.inno.stc14.service.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Logger;

import static java.util.Objects.nonNull;

@WebFilter("/*")
public class AuthFilter implements Filter {
    private Logger logger = Logger.getLogger(AuthFilter.class.getName());
    private UserService user;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.warning("init authFilter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        Connection connection = (Connection) request.getServletContext().getAttribute("DBConnection");
        HttpSession session = request.getSession();
        user = new UserServiceImpl(connection);
        if (nonNull(session.getAttribute("name")) && nonNull(session.getAttribute("password"))) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else  if (user.isExistUser(name, password)) {
            session.setAttribute("name", name);
            session.setAttribute("password", password);
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }
}