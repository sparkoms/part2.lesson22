package ru.inno.stc14.dao.jdbc;

import ru.inno.stc14.dao.UserDAO;
import ru.inno.stc14.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAOImpl implements UserDAO {
    private static Logger logger = Logger.getLogger(UserDAOImpl.class.getName());
    private final Connection connection;

    public UserDAOImpl(Connection connection) {
        this.connection = connection;
    }

    private static final String INSERT_USER_SQL_TEMPLATE =
            "insert into users (name, password) values (?, ?)";
    private static final String SELECT_USER_SQL_TEMPLATE =
            "select id, name, password from users";
    private static final String SELECT_USERANDPASSWORD_SQL_TEMPLATE =
            "select * from users where name = ? and password = ?";

    @Override
    public List<User> getList() {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_USER_SQL_TEMPLATE)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getInt(1));
                    user.setName(resultSet.getString(2));
                    user.setPassword(resultSet.getString(3));
                    users.add(user);
                }
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "An exception occurred on the DAO layer.", e);
        }
        return users;
    }

    @Override
    public boolean addUser(User user) {
        try (PreparedStatement statement = connection.prepareStatement(INSERT_USER_SQL_TEMPLATE)) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getPassword());
            statement.execute();
            return true;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "An exception occurred on the DAO layer.", e);
            return false;
        }
    }

    @Override
    public boolean isExistUser(User user) {
        try (PreparedStatement statement = connection.prepareStatement(SELECT_USERANDPASSWORD_SQL_TEMPLATE)) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getPassword());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "An exception occurred on the DAO layer.", e);
            return false;
        }
    }
}